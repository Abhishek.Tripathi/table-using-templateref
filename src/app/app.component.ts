import { AfterViewInit, ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DateRendererComponent } from './renderer/date-renderer/date-renderer.component';
import { DefaultRedererComponent } from './renderer/default-rederer/default-rederer.component';
import { NumberRendererComponent } from './renderer/number-renderer/number-renderer.component';
import { RendererStoreComponent } from './renderer/renderer-store/renderer-store.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

    // For renderers with Individualt component
    // @ViewChild('defaultRenderer') public default: DefaultRedererComponent | null = null;

    // For Renderers with a single store component
    // @ViewChild('rendererStore') public templateStore: RendererStoreComponent | null = null;

    @ViewChild('custom', { static: true}) custom: TemplateRef<any>;
    title = 'listing';

    listingData = [
        { id: 1, name: 'Abhishek', age: 22, birthDate: '23 Nov 1996', gender: 'Male'},
        { id: 2, name: 'Abhishek', age: 22, birthDate: '23 Nov 1996', gender: 'Male'},
        { id: 3, name: 'Abhishek', age: 22, birthDate: '23 Nov 1996', gender: 'Male'},
        { id: 4, name: 'Abhishek', age: 22, birthDate: '23 Nov 1996', gender: 'Male'}
    ]

    columnsType:Array<any> = []

    constructor(private _cdr: ChangeDetectorRef) {}

    ngOnInit() {
        this.columnsType = [
            { key: 'name', renderer: "default", rendererParams: { component: DefaultRedererComponent}},
            { key: 'age', renderer: "number", rendererParams: { component: NumberRendererComponent}},
            { key: 'birthDate', renderer: "date", rendererParams: { component: DateRendererComponent}},
            { key: 'gender', renderer: "custom",rendererParams: { template: this.custom }}
        ]
    }
    
    ngAfterViewInit() {
    }

    defaultTempClicked($event:any) {
        console.log(`Parent clicked ${$event.name}`)
    }

    _trackByKey(index:number,column: any) {
        return column.key;
    }

    shuffleColumnType() {
        for(let i=0; i<this.columnsType.length;i++) {
            let rndNumber = this.getRndInteger(0,this.columnsType.length - 1);
            let temp = this.columnsType[i];
            this.columnsType[i] = this.columnsType[rndNumber];
            this.columnsType[rndNumber] = temp;
        }
    }

    getRndInteger(min:number, max:number) {
        return Math.floor(Math.random() * (max - min) ) + min;
    }

    deleteLast() {
        this.columnsType.splice(this.columnsType.length - 1);
    }


}
