import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListingComponent } from './listing/listing.component';
import { DefaultRedererComponent } from './renderer/default-rederer/default-rederer.component';
import { ListingColComponent } from './listing/listing-col/listing-col.component';
import { NumberRendererComponent } from './renderer/number-renderer/number-renderer.component';
import { DateRendererComponent } from './renderer/date-renderer/date-renderer.component';

@NgModule({
    declarations: [
    AppComponent,
    ListingComponent,
    ListingColComponent,
    ],
    imports: [
    BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    entryComponents: [DefaultRedererComponent, NumberRendererComponent,DateRendererComponent]
})
export class AppModule { }
