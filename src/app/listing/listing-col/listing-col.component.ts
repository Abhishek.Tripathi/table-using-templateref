import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
    selector: 'app-listing-col',
    templateUrl: './listing-col.component.html',
    styleUrls: ['./listing-col.component.css']
})
export class ListingColComponent implements OnInit {

    @ContentChild(TemplateRef) columnTemplate: TemplateRef<any> | null = null;

    @Input() column: any;
    
    constructor() { }
    
    ngOnInit(): void {
    }
    
}
