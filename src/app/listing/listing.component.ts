import { 
    AfterViewInit, 
    ChangeDetectionStrategy, 
    ChangeDetectorRef, 
    Component, 
    ComponentFactoryResolver, 
    ComponentRef, 
    ContentChildren, 
    Input, 
    OnDestroy, 
    OnInit, 
    QueryList, 
    TemplateRef, 
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators'
import { ListingColComponent } from './listing-col/listing-col.component';

@Component({
    selector: 'app-listing',
    templateUrl: './listing.component.html',
    styleUrls: ['./listing.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListingComponent implements OnInit, AfterViewInit, OnDestroy {

    @ContentChildren(ListingColComponent) private _listingColumns: QueryList<ListingColComponent> | undefined;
    @ViewChild('cellContentSibling', { read: ViewContainerRef }) cellContentSibling: ViewContainerRef;

    // @Input() public data: Array<any> = [];
    @Input() set data(v: Array<any>) {
        if(v) {
            this._data$.next(v);
        }
    }
    @Input() set columnTypes(v: any) {
        if(v) {
            this.createTemplates(v);
        }
    }
    // public columns: ListingColComponent[] = [];
    private _columns$: BehaviorSubject<any> = new BehaviorSubject([]);
    private _data$: BehaviorSubject<any> = new BehaviorSubject([]);
    private _destroy$ = new Subject();
    private _templateMapper$: BehaviorSubject<any> = new BehaviorSubject({});

    public vm$: Observable<any> = new Observable();

    constructor(private _cdr: ChangeDetectorRef, private _componentFactoryResolver: ComponentFactoryResolver, private _vf: ViewContainerRef) { }
    
    ngOnInit(): void {
        this.vm$ = combineLatest([this._columns$, this._data$, this._templateMapper$]).pipe(
            map(([columns,data,templateMapper]) => ({ columns,data,templateMapper}))
        )
    }

    ngAfterViewInit(): void {
        this.refreshListing();
        this.enableCoulmnChangesListener();
    }
    
    enableCoulmnChangesListener(): void {
        if(this._listingColumns)
            this._listingColumns.changes.pipe(takeUntil(this._destroy$)).subscribe(_ => {
                this.refreshListing();
            })
    }

    refreshListing(): void {
        if(this._listingColumns) {
            this._columns$.next(this._listingColumns.toArray())
            // this.columns = this._listingColumns.toArray();
            // this._cdr.detectChanges();
        }
    }

    trackById(index: number,item:any) {
        return item.id;
    }

    _trackByKey(index: number,column: any) {
        // console.log('Hii');
    }

    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }

    createItem() {
        console.log('hii');
    }

    createTemplates(columnTypes:any) {
        let value = this._templateMapper$.value;
        for(let column of columnTypes) {
            if( (!value || !value[column.renderer] || !value[column.renderer].template) && column.renderer != 'custom') {
                let resolver = this._componentFactoryResolver.resolveComponentFactory(column.rendererParams['component']);
                let factory: ComponentRef<any> = this._vf.createComponent(resolver);
                // this.templateMapper[column.renderer].template = setTimeout()
                if(!value[column.renderer]) {
                    value[column.renderer] = { component: column.rendererParams['component'], template: null }
                }
                this._templateMapper$.next(value);
                setTimeout(() => {
                    value[column.renderer].template = factory.instance.template;
                    this._templateMapper$.next(value);
                },0)
            }
        }
    }
}

export interface ITemplateMapper {
    default?: { 
        component?: ComponentRef<any>, 
        template?: TemplateRef<any> | null
    },
    number?: {
        component?: ComponentRef<any>, 
        template?: TemplateRef<any> | null
    }
}

// export enum CellRenderers {
//     "default" = new DefaultRedererComponent(),

// 
