import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-date-renderer',
  templateUrl: './date-renderer.component.html',
  styleUrls: ['./date-renderer.component.css']
})
export class DateRendererComponent implements OnInit {

    @ViewChild('template', { static: true }) template: TemplateRef<any>;

    constructor() { }
    
    ngOnInit(): void {
    }

}
