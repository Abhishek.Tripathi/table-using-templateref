import { Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';

@Component({
    selector: 'app-default-rederer',
    templateUrl: './default-rederer.component.html',
    styleUrls: ['./default-rederer.component.css']
})
export class DefaultRedererComponent implements OnInit {
    @ViewChild('default', { static: true }) public template: TemplateRef<any>;
    @Output('defaultTempClicked') private _divClicked: EventEmitter<any> = new EventEmitter<any>();

    constructor() { }
    
    ngOnInit(): void {
    }
    divClicked($event:any) {
        console.log('Default clicked',$event.id);
        this._divClicked.emit($event);
    }

    getStringify(column: any) {
        return JSON.stringify(column);
    }
}
