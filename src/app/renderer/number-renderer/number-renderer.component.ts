import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
    selector: 'app-number-renderer',
    templateUrl: './number-renderer.component.html',
    styleUrls: ['./number-renderer.component.css']
})
export class NumberRendererComponent implements OnInit {

    @ViewChild('number', { static: true}) template: TemplateRef<any>;

    constructor() { }
    
    ngOnInit(): void {
    }

}
