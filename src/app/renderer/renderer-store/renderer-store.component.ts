import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-renderer-store',
  templateUrl: './renderer-store.component.html',
  styleUrls: ['./renderer-store.component.css']
})
export class RendererStoreComponent implements OnInit {

    @ViewChild('date') date: TemplateRef<any> | null = null;
    @ViewChild('number') number: TemplateRef<any> | null = null;

  constructor() { }

  ngOnInit(): void {
  }

}
